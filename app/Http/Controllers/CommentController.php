<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Auth;
use App\Post;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Comment::all();
        return view('comment.comment-list', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request["post_id"]);
        // $data = new Comment;
        // $data->content = $request['comment'];
        // $data->user_id = Auth::id();
        // $data->post_id = $data->made();
        // $data->save();

        $post = Post::find($request["post_id"]);
        $comment = $post->comments()->create([
            "content" =>$request["comment"],
            "user_id" =>Auth::id()
        ]);

        Alert::success('Berhasil!', 'Komentar baru ditambahkan!');

        return redirect('/posts');
        // ->with('success', 'Komentar baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        Comment::destroy($id);

        Alert::success('Berhasil!', 'Komentar berhasil dihapus!');

        return redirect('/posts');
        // ->with('success', 'Komentar Berhasil Dihapuss!');
    }
}
