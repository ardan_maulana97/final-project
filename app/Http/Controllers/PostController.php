<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Auth;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data = Post::all();
        $liked_posts = [];
        $liked_comments = [];

        // like posts
        foreach($data as $d){
            foreach($d->post_likes as $like){
                if($like->user_id == Auth::id()){{
                    $liked_posts[] = $d->id;
                }}
            }
        }

        // comment posts
        foreach($data as $d){
            foreach($d->comments as $comment){
                foreach($comment->comment_likes as $like){
                    if($like->user_id == Auth::id()){{
                        $liked_comments[] = $comment->id;
                    }}
                }
            }
        }

        return view('post.post-list', compact('data', 'liked_posts', 'liked_comments'));
    }

    public function create(){
        return view('post.create-post');
    }

    public function store(Request $request){

        $data = new Post;


        if($request->hasFile('photo')){
            $filename = $request['photo']->getClientOriginalName();
            if( $data->photo ){
                Storage::delete('/public/storage/posts/'.Auth::user()->profile->photo);
            }
            $request['photo']->storeAs('posts', $filename, 'public');
        }else{
            $filename=$data->photo;
        }

        $data->title = $request['title'];
        $data->content = $request['content'];
        $data->photo = $filename;
        $data->user_id = Auth::id();
        $data->save();

        Alert::success('Berhasil!', 'Post baru berhasil ditambahkan!');

        return redirect('/posts');
        // ->with('success', 'Post baru berhasil ditambahkan!');
    }

    public function my_post(){
        $post = new Post;
        $user_id = Auth::id();
        $data = $post->where('user_id', $user_id)->get();
        return view('post.my-post', compact('data'));
    }

    // public function show($id){
    //     $data = Post::find($id);
    //     return view('post.my-post', compact('data'));
    // }

    public function edit($id){
        $data = Post::find($id);
        return view('post.edit-post', compact('data'));
    }

    public function update($id, Request $request){
        if($request->hasFile('photo')){
            $filename = $request['photo']->getClientOriginalName();

            if( Post::find($id)->photo ){
                Storage::delete('/public/storage/posts/'.Post::find($id)->photo);
            }
            $request['photo']->storeAs('posts', $filename, 'public');
        }else{
            $filename=Post::find($id)->photo;
        }
        $data = Post::where('id', $id)->update([
            'title' => $request['title'],
            'content' => $request['content'],
            'photo' => $filename
        ]);

        Alert::success('Berhasil!', 'Post berhasil diperbaharui!');

        return redirect('/posts');
        // ->with('success', 'Data berhasil diperbaharui!');
    }

    public function destroy($id){
        $data = Post::find($id);
        $data->delete();

        Alert::success('Berhasil!', 'Post berhasil dihapus!');

        return redirect('/posts');
        // ->with('success', 'Data berhasil dihapus!');
    }

    public function getPubliclyStorgeFile($filename)

    {
        $path = storage_path('app/public/image/'. $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);

        $response->header("Content-Type", $type);

        return $response;

    }


}
