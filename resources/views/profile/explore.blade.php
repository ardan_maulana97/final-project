@extends('layout.master')
@section('content')
<div class="container">
    <div class="row">
        @forelse($profiles as $profile)

                <div class="card card-primary card-outline col-sm-3 mt-3 ml-3">
                <div class="card-body box-profile">
                    <div class="text-center">
                    @if($profile->photo)
                        <img class="profile-user-img img-fluid img-circle" src=" {{ asset('storage/photo/'.$profile->photo) }} " alt="User Avatar">
                    @else
                        <img class="profile-user-img img-fluid img-circle" src=" {{ asset('assets/dist/img/user1-128x128.jpg') }}" alt="User Avatar">
                    @endif
                    </div>

                    <h3 class="profile-username text-center">{{ $profile->fullname }}</h3>

                    <!-- <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Followers</b> <a class="float-right">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>Following</b> <a class="float-right">543</a>
                    </li>
                    </ul> -->
                    @if( in_array($profile->user_id, $followed_users_ids) )
                        <a href="explore/{{$profile->user_id}}/follow" class="btn btn-outline-primary btn-block"><b>Unfollow</b></a>
                    @else
                        <a href="explore/{{$profile->user_id}}/follow" class="btn btn-primary btn-block"><b>Follow</b></a>
                    @endif
                </div>
                <!-- /.card-body -->
                </div>
        @empty
            No Users
        @endforelse
    </div>
</div>

@endsection