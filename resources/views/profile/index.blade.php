@extends('layout.master')

@section('content')

<div class="container mt-3">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="card card-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white" style="background: url('{{ asset('assets/dist/img/photo1.png') }}') center center;">
                <h3 class="widget-user-username text-right">{{ $profile->fullname }}</h3>
                <h5 class="widget-user-desc text-right">{{ $profile->address }}</h5>
              </div>
              <div class="widget-user-image">
              @if(Auth::user()->profile->photo)
                <img class="img-circle" src=" {{ asset('storage/photo/'.$profile->photo) }} " alt="User Avatar">
              @else
                <img class="img-circle elevation-2" src=" {{ asset('assets/dist/img/user1-128x128.jpg') }}" alt="User Avatar">
              @endif
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">{{ $count_post }}</h5>
                      <span class="description-text">POSTS</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">{{ $follower }}</h5>
                      <span class="description-text">FOLLOWERS</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">{{ $following }}</h5>
                      <span class="description-text">FOLLOWING</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
    </div>
    <div class="card card-primary">
        <div class="card-header d-flex justify-content-between">
            <h3 class="card-title">About Me</h3>
            <a class="btn btn-primary" href=" {{ route('profile.edit', [ 'profile' => $profile->id ]) }} ">
                <i class="fas fa-edit"></i> Edit
            </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <strong><i class="fas fa-user-alt mr-1"></i> Full Name </strong>

            <p class="text-muted">
                {{ $profile->fullname }}
            </p>

            <hr>

            <strong><i class="fas fa-map-marker-alt mr-1"></i> Address</strong>

            <p class="text-muted">{{ $profile->address }}</p>

            <hr>

            <strong><i class="fas fa-phone-alt mr-1"></i> Phone Number</strong>

            <p class="text-muted">
                <span class="tag tag-danger">+{{ $profile->phone }}</span>
            </p>

            <hr>

        </div>
        <!-- /.card-body -->
    </div>
</div>

@endsection