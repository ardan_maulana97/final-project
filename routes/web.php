<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\StorageFileController;

Route::get('image/{filename}', [StorageFileController::class,'getPubliclyStorgeFile'])->name('image.displayImage');

// route profile
Route::resource('profile', 'ProfileController');
Route::get('/explore', 'ProfileController@explore');
Route::get('/explore/{id}/follow', 'ProfileController@follow');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'PostController@index');


// route posts
Route::get('/', function(){
    return view('welcome');
});

Route::resource('posts', 'PostController');

Route::resource('comments', 'CommentController');

Route::get('/my_post', 'PostController@my_post')->name('posts.my_post');

Route::get('/my_comment', 'CommentController@my_comment')->name('comments.my_comment');


//like posts
Route::resource('postlike', 'PostLikeController');

//like comment
Route::resource('commentlike', 'CommentLikeController');

